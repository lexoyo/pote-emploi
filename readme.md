~this project's doc is in french since it is an attempt at making an API for the french website for unemployed people~

# Pote emploi

Un robot qui se log sur https://candidat.pole-emploi.fr/ avec mes identifiants et qui me donne toutes les info utiles.

Ce projet est financé par Pôle Emploi, d'une certaine manière (détournée)...

# Install

```
$ npm install
```


# Use as a node module

See in [the CLI app](./app/pote-emploi-cli.js)


# Use standalone

Run

```
$ node app/pote-emploi-cli.js -u 1234567x -p 123456 -c 12345 auth
$ node app/pote-emploi-cli.js -u 1234567x -p 123456 -c 12345 status
$ node app/pote-emploi-cli.js -u 1234567x -p 123456 -c 12345 update
```

Result of getStatus

```
{
  situDate: 2017-02-23T00:00:00.000Z,
  subDateStart: 2017-04-28T00:00:00.000Z,
  subDateEnd: 2017-05-15T00:00:00.000Z,
  daily: null,
  updated: false,
  updatedLink: null,
  updateTodo: true,
  updateTodoStart: 2017-04-28T00:00:00.000Z,
  updateTodoEnd: 2017-05-15T00:00:00.000Z,
  isRegistered: true,
  updateLink: 'https://candidat.pole-emploi.fr/candidat/espacepersonnel/regroupements/index.popinnouvelespace.valider'
}
```

