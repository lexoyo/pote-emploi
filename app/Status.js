let request = require('request');
const path = require('path');
const { JSDOM } = require('jsdom');

class Status {
  constructor(cookies) {
    request = request.defaults({ jar : cookies });
  }
  get(cbk) {
    request('https://candidat.pole-emploi.fr/candidat/espacepersonnel/regroupements', (err, response, body) => {
      if (err) {
        cbk(err);
      }
      else {
        const dom = new JSDOM(body);
        const document = dom.window.document;
        const features = document.querySelectorAll('.body-unit .u-feature .bd p');
        const updateLinkEl = document.querySelector('.nav .primary-button a');

        const status = {
          situDate: null,
          subDateStart: null,
          subDateEnd: null,
          daily: null,
          updated: null,
          updatedLink: null,
          updateTodo: null,
          updateTodoStart: null,
          updateTodoEnd: null,
          isRegistered: null,
        };

        // update link
        if(updateLinkEl) {
          status.updateLink = 'https://candidat.pole-emploi.fr' + updateLinkEl.href;
        }

        // 2 sections on the home page
        const maSituation = features[0].innerHTML;
        const monActualisation = features[1].innerHTML;

        // extraction of the subscribe date
        const matchDateSitu = maSituation.match(/depuis le ([0-9]*)\/([0-9]*)\/([0-9]*)/);
        if(matchDateSitu) {
          status.situDate = new Date(matchDateSitu[3] + '-' + matchDateSitu[2] + '-' + matchDateSitu[1]);
        }

        // extraction of the subscribtion type
        const matchDateSub = monActualisation.match(/entre le ([0-9]*)\/([0-9]*)\/([0-9]*) et le ([0-9]*)\/([0-9]*)\/([0-9]*)/);
        if(matchDateSub) {
          status.subDateStart = new Date(matchDateSub[3] + '-' + matchDateSub[2] + '-' + matchDateSub[1]);
          status.subDateEnd = new Date(matchDateSub[6] + '-' + matchDateSub[5] + '-' + matchDateSub[4]);
        }

        // update status
        const matchUpdateOk = monActualisation.match(/a été validée/);
        status.updated = !!matchUpdateOk;

        const matchUpdateTodo = monActualisation.match(/Votre actualisation doit s\'effectuer entre le ([0-9]*)\/([0-9]*)\/([0-9]*) et le ([0-9]*)\/([0-9]*)\/([0-9]*)/);
        status.updateTodo = !!matchUpdateTodo;
        if(matchUpdateTodo) {
          status.updateTodoStart = new Date(matchUpdateTodo[3] + '-' + matchUpdateTodo[2] + '-' + matchUpdateTodo[1]);
          status.updateTodoEnd = new Date(matchUpdateTodo[6] + '-' + matchUpdateTodo[5] + '-' + matchUpdateTodo[4]);
        }

        // registered
        const matchNotRegistered = monActualisation.match(/Réalisez votre demande de réinscription/);
        status.isRegistered = !matchNotRegistered;

        // extraction du paiement journalier
        const dailyMatch = monActualisation.match(/indemnisation est de ([0-9]*)€/);
        if(dailyMatch) {
          status.daily = parseFloat(dailyMatch[1]);
        }
        cbk(null, status);
      }
    });
  }
}

module.exports = Status;

