let request = require('request');
const path = require('path');
const { JSDOM } = require('jsdom');

class Updater {
  constructor(cookies) {
    request = request.defaults({ jar : cookies });
  }
  update(updateLink, cbk) {
    request(updateLink, (err, response, body) => {
      if (err) {
        cbk(err);
      }
      else {
        cbk('Error: not yet implemented!');
      }
    });
  }
}
module.exports = Updater;
