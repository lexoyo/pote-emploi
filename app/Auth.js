let request = require('request');
const path = require('path');
const { JSDOM } = require("jsdom");

class Auth {
  /**
   * constructor, store the provided options
   * @param  {Object} opts, an object with user, password, postcode
   */
  constructor(opts, cookies) {
    this.user = opts.user;
    this.password = opts.password;
    this.postcode = opts.postcode;
    request = request.defaults({ jar : cookies });
  }
  getUrlStep1() {
    return 'https://candidat.pole-emploi.fr/candidat/espacepersonnel/authentification';
  }
  // getUrlPreAuthStart() {
  //   return 'https://candidat.pole-emploi.fr/candidat/espacepersonnel/authentification/index.formulaire';
  // }
  login(cbk) {
    request.cookie('authentification-accessible=true');
    request(this.getUrlStep1(), (err, response, body) => {
      if (err) {
        cbk(err);
      }
      else {
        const dom = new JSDOM(body);
        const document = dom.window.document;
        const inputs = document.querySelectorAll('input');
        const postData = {};
        inputs.forEach(el => {
          if(el.type === 'hidden') {
            postData[el.name] = el.value;
          }
        });
        postData.champTexteIdentifiant = this.user;
        const form = document.querySelector('form#formulaire');
        this.step2('https://candidat.pole-emploi.fr' + form.action, postData, cbk);
      }
    });
  }
  step2(url, postData, cbk) {
    request.post(
      {url: url, formData: postData, followAllRedirects: true},
      (err, httpResponse, body) => {
        if (err) {
          cbk(err);
        }
        else {
          const dom = new JSDOM(body);
          const document = dom.window.document;

          const inputs = document.querySelectorAll('input');
          const postData = {};
          inputs.forEach(el => {
            if(el.type === 'hidden') {
              postData[el.name] = el.value;
            }
          });
          postData.champMotDePasse = this.password;
          postData.champTexteCodePostal = this.postcode;
          const form = document.querySelector('form#formulaire');
          // this.step3(form.action, postData, cbk);
          // this.step3('https://candidat.pole-emploi.fr/candidat/espacepersonnel/authentification/index.formulaire', postData, cbk);
          this.step3('https://candidat.pole-emploi.fr' + form.action, postData, cbk);
        }
    });
  }
  step3(url, postData, cbk) {
    request.post(
      {url: url, formData: postData, followAllRedirects: true},
      (err, httpResponse, body) => {
        if (err) {
          cbk(err);
        }
        else {
          const dom = new JSDOM(body);
          const document = dom.window.document;

          if(body.indexOf('Ma situation') > 0) {
            console.info('Authentication Success');
            cbk();
          }
          else {
            console.info('Authentication Failed (unknown reason)');
            cbk('Authentication Echouée (unknown reason)');
          }
        }
    });

  }
  display(html, filename) {
    const escaped = html; //.replace(/"/g, '\\"');
    const outPath = path.resolve('/tmp/', filename || 'out.tmp');
    const fs = require('fs');
    const inPath = path.resolve('/tmp/', 'in.tmp');
    fs.writeFileSync(inPath, escaped);
    const shell = require('shelljs');
    shell.config.silent = true;
    shell.exec('w3m -dump -T text/html ' + inPath)
    .to(outPath);
  }
}

module.exports = Auth;

