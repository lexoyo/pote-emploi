const Auth = require('./Auth');
const Status = require('./Status');
const Updater = require('./Updater');
const FileCookieStore = require('tough-cookie-filestore');
const fs = require('fs');

class PoteEmploi {
  /**
   * constructor, store the provided options
   * @param  {Object} opts, an object with user, password, postcode
   */
  constructor(opts) {
    this.user = opts.user;
    this.password = opts.password;
    this.postcode = opts.postcode;

    const filepath = 'cookies.json';
    if(!fs.existsSync(filepath)) fs.closeSync(fs.openSync(filepath, 'w'));
    this.cookies = new FileCookieStore(filepath);
  }
  auth(cbk) {
    const auth = new Auth({
      user: this.user,
      password: this.password,
      postcode: this.postcode,
    }, this.cookies);
    auth.login(cbk);
  }
  getStatus(cbk) {
    const status = new Status(this.cookies);
    status.get(cbk);
  }
  update(updateLink, cbk) {
    const updater = new Updater(this.cookies);
    updater.update(updateLink, cbk);
  }
}
module.exports = PoteEmploi;
