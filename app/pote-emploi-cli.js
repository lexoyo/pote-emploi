const program = require('commander');
const path = require('path');
const colors = require('colors');
const package = require(path.resolve(__dirname, '../package.json'));
const PoteEmploi = require('./');


function error(...args) {
  console.error(colors.red('\n  ' + args.join(' ')));
  program.help();
  process.exit(1);
}

program
  .version(package.version)
  .arguments('<cmd>')
  .option('-u, --user <Identifiant pole-emploi>', 'Identifiant pole-emploi')
  .option('-p, --password <Mot de passe>', 'Mot de passe pole-emploi')
  .option('-c, --postcode <Code postal>', 'Code postal (wtf pole emploi)')
  .action(function (cmd) {
     cmdValue = cmd;
  });

program.parse(process.argv);

if (typeof cmdValue === 'undefined') {
  error('no command given!');
}
if (typeof program.user === 'undefined') {
  error('-u option required');
}
if (typeof program.password === 'undefined') {
  error('-p option required');
}
if (typeof program.postcode === 'undefined') {
  error('-c option required');
}

var poteEmploi = new PoteEmploi({
  user: program.user,
  password: program.password,
  postcode: program.postcode,
});
switch(cmdValue) {
  case 'auth':
    console.log('auth...');
    poteEmploi.auth((error) => console.log('error:', error));
  break;
  case 'status':
    console.log('status... FIXME: should load cookies and not run auth again');
    poteEmploi.auth((error) => {
      if(error) {
        console.log('error:', error)
      }
      else {
        poteEmploi.getStatus((error, status) => console.log('error:', error, 'status:', status));
      }
    });
  break;
  case 'update':
    console.log('status... FIXME: should load cookies and not run auth again');
    poteEmploi.auth((error) => {
      if(error) {
        console.log('error:', error)
      }
      else {
        poteEmploi.getStatus((error, status) => {
          console.log('error:', error, 'status:', status);
          poteEmploi.update(status.updateLink, (error) => console.log('error:', error));
        });
      }
    });
    break;
  default:
    error('Unknown command', cmdValue);
}
