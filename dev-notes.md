> à voir
> * https://play.google.com/store/apps/details?id=com.poleemploi.pemobile
> * APIs pole emploi (super roots) http://www.emploi-store-dev.fr/portail-developpeur/espacedonneesdoctechnique

### known bugs

* tous les liens de download des courriers sont les memes
  => c'est juste un bug de sélecteur car quand je regarde dans mon espace j'ai bien des docs différents
* dates de prochaine actu n'a pas fonctionné
  => vérifier avec 
    ```
    Mon actualisation :  Votre actualisation pour la période du 01/07/2015 au 31/07/2015 a été validée.
    Vous devrez réaliser votre prochaine actualisation entre le 28/08/2015 et le 15/09/2015.
    ```

* ca n'actualise pas à tous les coups, j'ai du le lancer 2 fois, j'ai eu exactement les memes logs, mais seulement la 2e fois ca a marché
  => aller jusqu'à l'ecran suivant et le screenshot + télécharger le pdf
    ```
    Déclarez votre situation mensuelle

    Confirmation

    Votre déclaration de situation mensuelle a bien été validée le 30/07/2015 à 14h34

    Vous avez la possibilité de la modifier jusqu'à 17:00.
    Elle sera prise en compte sous un délai de 24h par Pôle emploi.

    Si vous souhaitez un justificatif, téléchargez le fichier ci-dessous, avec Adobe Reader, et imprimez-le :

         Visualiser / Imprimer le justificatif d'actualisation (200Ko)*
    ```

  => retourner sur "m'actualiser" pour vérifier qu'on a bien
    ```
    Déclaration pour la période du 01/07/2015 au 31/07/2015

    Vous avez déjà déclaré votre situation pour cette période le 30/07/2015 à 14h08.
    Cette nouvelle déclaration annulera et remplacera la précédente.
    ```



## Idées prochaines étapes :

User management

Avec mongodb

Non, trop relou :

* use redis 
* store the users and their creds and info from pole-emploi.fr
* have an ordered list of ids for users to be handled
* have a list per server to store pending users (use [rpoplpush](http://redis.io/commands/rpoplpush) to move users back and forth between users list and servers lists)

### API

* /user
  > info sur l'utilisateur

* /mail
  > liste des courriers / échanges avec nos potes emploi

* /mail/:idx/download
  > télécharger un courrier

* /status
  > status de l'actualisation et des paiements

* /status/update
  > s'actualiser

### refaire le site pôle emploi en mieux

* inscription des utilisateurs
* faire tourner le script tous les jours
* envoi par email des courriers reçus (en PJ - [lib de mail sympa](http://www.nodemailer.com/#attachments))
* envoi par email lorsqu'un paiement est effectué
* effectuer mon "actualisation" mensuelle automatiquement
